About This Release

This is a WinMerge 2.12.2 stable release. This release replaces earlier WinMerge stable releases as a recommended release.

Please submit bug reports to our bug-tracker.

What Is New in 2.12.2?

WinMerge 2.12.2 is an bug-fix release fixing couple of important bugs found from the 2.12.0 release. For a complete changelog history, see the ChangeLog.txt file located in the Docs folder under the WinMerge program folder.

Bugs fixed

Disable folder compare tree-view by default
Filename extension column was empty for files (in folder compare)
Crash when selecting backup folder
Swapping panes did not update statusbar (in file compare)
Says files are identical after making files different in another application and re-loading them
Files with missing last EOL (End Of Line) bytes were not merged correctly
Translation updates

Polish
What Is New in 2.12.0?

WinMerge 2.12 is an important update to WinMerge. There are lots of improvements and bug fixes since 2.10.4 stable release.

2.12.0 stable release has only couple of changes after beta release. See the 2.12 beta release changes below for more complete change list in this release.

Improvements

Remember last target folder for Copy/Move to in folder compare
Bugs fixed

Help window opened from WinMerge was modal
Files without extension get backed up to name..bak
Translation updates

Chinese Traditional
Croatian
Czech
Danish
Dutch
French
Galician
German
Greek
Japanese
Swedish
Ukrainian
What Is New in 2.11.2 beta?

WinMerge 2.11.x adds couple of long awaited features. Overall WinMerge 2.11.x will be important improvement for some use cases.

Tree-view for folder compare

WinMerge can now show folder compare results in a tree-style view. This is much improved way to show result compared to old flat result view. Subfolders in tree-view can be made visible and hidden like in Windows Explorer. The tree-style is only available when subfolders are included to the comparison.

Include items in unique folders in compare results

If subfolders are included to the compare all unique subfolders and files in them are now visible in compare results. In earlier versions WinMerge only shows the top-level unique folder. This means one can copy subfolders and files inside unique folders to another side and proper folder structure is created in the target side.

General compare result statuses

When WinMerge cannot determine if file is text- or binary-file it now shows the compare status with general statuses. Earlier WinMerge versions showed such files as text files. This is important since e.g. file size/time -based compare methods cannot know if file is binary- or text-file.

More accurate location pane

Location pane now shows missing lines more accurately. Also the visible area indicator positioning matches better reality.

Re-load files changed in disk behind WinMerge

WinMerge now finally can re-load files that have been changed in the disk after WinMerge loaded them. This is remarkable usability improvement since earlier versions required user to close current files and open them again.

Other changes

Refresh compare results after changing line filter
Add option to show Open-dialog on WinMerge startup
PCRE updated to version 7.8
Installer can add WinMerge to system path
New options page for Shell Integration options
Better Unicode file detection for full contents compare
Important bug fixes

Quick compare didn't ignore EOL differences
Remove invalid characters from command line
Crash when compared file disappeared during file compare
Don't show file name extension for folders
Known Issues

7-zip installed from MSI installer may not work with WinMerge.

Workaround: Use the EXE installer of 7-Zip. See manual 4.5. 7-Zip and Archive Support for more information.

Old IE version can cause crashes when handling archive files.

Workaround: If WinMerge crashes when handling archives, consider updating IE to version 6.0 or later.

Supercopier application causes WinMerge to hang.

Supercopier application causes WinMerge to not exit properly and leaves WinMerge process hang. See the bug reports:

#1976241 the winmerge.exe process doesn't stop itself
#1602313 WinMerge stays in tasklist after closing
Workaround: Disable the Supercopier when using WinMerge.

There is no way to specify the codepage for a particular file.

Workaround: Specify the default codepages for all files in Edit->Options->Codepage->Custom codepage.

There is no 3-way merge. #990464

Filters only applied when using full compare.

Line filtering is only applied in folder compare when using Full Contents-compare method.

If you are using any other compare method, line filters are not applied. Files marked different in folder compare can get status changed to identical when opening them to file compare.

Some plugins shipped with WinMerge require Visual Basic runtime library to work

Workaround: You can download the required file msvbvm60.dll or use the Run-Time Redistribution Pack from Microsoft.

Workaround 2: If you don't need the plugin, you can safely remove it.

VSS integration cannot add new files to VSS

If you need to add a new file, you must add it via VSS client program.

Unset WinMerge integration from ClearCase

To remove WinMerge integration, you'll need to edit one file by hand:

Find ClearCase installation folder
Usually it is something like C:\Program Files\Rational\ClearCase
Go to subfolder lib\mgrs\
Open file map in there to editor program
Comment-out the line starting with "text_file_delta xcompare", containing "WinMergeU.exe"
Uncomment the line starting with "text_file_delta xcompare", containing "cleardiffmrg.exe"
