import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

//import org.w3c.dom.*;

import nu.xom.*;




public class srcMLParser {

	public static final XPathContext namespace = new XPathContext("srcml",
		    "http://www.srcML.org/srcML/src");
	
	//Class that holds the identifiers for Classes/Methods/Attributes 
	public SourceIdentifiers srcIdentifiers = new SourceIdentifiers();
	
	srcMLParser()
	{
		
	}
	
	//Method that gets all the xml files that srcML produced are returns their file path in a list
	public List<String> populateXMLFileList()
	{
		List<String> fileList = new ArrayList<String>();
		
		//Destination folder for srcML files.  One is for winMerge and the other is for fileZilla
		//In the future, plan to control it so both are split and candidate sentences instead of just one at a time...
		//File srcMLDest = new File("/home/alex/workspace/Research/Resources/srcMLOutput/winMergeSrc");
		File srcMLDest = new File("/home/alex/workspace/Research/Resources/srcMLOutput/fileZillaSrc");
		File[] listOfFiles = srcMLDest.listFiles();
		
		for(File file: listOfFiles)
		{
			if(file.isFile())
			{
				System.out.println("Files: " + file.getName());
				fileList.add(srcMLDest + "/" + file.getName());
			}
		}
		
		return fileList;
	}
	
	//Method that extracts identifiers for Classes/Methods/Attributes
	public SourceIdentifiers extractClassesAndTheirMethods(String filePath)
	{
		
		System.out.println("ParseXMLFile: FilePath = " + filePath);
		File xmlFile = new File(filePath);
		
		System.out.println("----------------------------------------------------------------");
		
		if(!(xmlFile.exists()))
		{
			System.out.println("File does not exist!: " + xmlFile);
			return null;
		}
		else
		{
			System.out.println("File Exists!: " + xmlFile);
		}
		
		try 
		{
			//Open XML file for parsing
			Builder builder = new Builder();
			Document doc = builder.build(xmlFile);
			System.out.println("Document: " + filePath + " is opened for parsing");
			
			srcIdentifiers.setFileName(filePath);
			
			//Get root node
			Element root = doc.getRootElement();
			System.out.println("Root Node: " + root.getLocalName());
			
			
			
			//Set namespace
			boolean isCpp = false;
			Nodes type = doc.query(".//srcml:unit/@language", namespace);
			String language = type.get(0).getValue();
			System.out.println("type: " + language);
			if(language.equals("C++"))
			{
				isCpp = true;
			}
			
			//Get Children Nodes
			//Gets Orphan Attributes nodes, orphan method nodes, and class nodes
			System.out.println("Children...");
			Nodes orphanAttrNodes = getOrphanAttributes(doc);
			Nodes orphanMethNodes = getOrphanMethods(doc);
			Nodes classNodes = getClassNodes(doc);
			
			//Just prints out how many were extracted
			System.out.println("Number of Orphan Atrributes:  " + orphanAttrNodes.size());
			System.out.println("Number of Orphan Methods: " + orphanMethNodes.size());
			System.out.println("Number of Classes: " + classNodes.size() + "\n");
			
			//Get each "Attribute" node
			getAttributeIDs(orphanAttrNodes, isCpp);
			
			//Get each "Method" node
			getMethodIDs(orphanMethNodes, isCpp);
			
			//Get each "Class" node
			String result = new String();
			result = getClassIDs(classNodes, isCpp);
			
			System.out.println("result:\n " + result);
		} 
		catch (ValidityException e) 
		{
			System.out.println("Validity Exeption!");
			e.printStackTrace();
		} 
		catch (ParsingException e) 
		{
			System.out.println("Parsing Exeption!");
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			System.out.println("IO Exeption!");
			e.printStackTrace();
		}
		return srcIdentifiers;
	}

	//Method that gets orphan method nodes and returns them
	public Nodes getOrphanMethods(Document doc) {
		Nodes orphanMethNodes = doc.query(".//srcml:function[not(ancestor::srcml:class)] | .//srcml:function_decl[not(ancestor::srcml:class)]", namespace);
		return orphanMethNodes;
	}

	//Method that gets orphan attribute nodes and returns them
	public Nodes getOrphanAttributes(Document doc) {
		Nodes orphanAttrNodes = doc.query(".//srcml:decl_stmt[not(ancestor::srcml:class|ancestor::srcml:function|ancestor::srcml:function_decl)]", namespace);
		return orphanAttrNodes;
	}

	//Method that gets class nodes and returns them
	public Nodes getClassNodes(Document doc) {
		Nodes classNodes = doc.query(".//srcml:class", namespace);
		return classNodes;
	}
	
	//Extract the identifiers from attribute nodes
	public void getAttributeIDs(Nodes attributeNodes, boolean isCpp)
	{
		for(int attrNum = 0; attrNum < attributeNodes.size(); attrNum++)
		{
			SrcAttribute attr = new SrcAttribute();
			Node attrNode = attributeNodes.get(attrNum);
			//System.out.println("Orphan Attributes: " + attrNode.getValue());
			Nodes nameOfAttr = attrNode.query("./srcml:decl/srcml:name", namespace);
			Nodes typeOfAttr = attrNode.query("./srcml:decl/srcml:type", namespace);
			System.out.println("Name of Attribute: " + nameOfAttr.get(0).getValue());
			System.out.println("Type of Attribute: " + typeOfAttr.get(0).getValue());
			attr.name = nameOfAttr.get(0).getValue();
			attr.type = typeOfAttr.get(0).getValue();
			//add these values to srcIdentifiers.attrubutes
			//need name and arg type (if bool)
			srcIdentifiers.attributes.add(attr);
		}
	}
	
	//Extract the method identifiers from the method nodes
	//Also extracts identifiers from attributes found in a method
	public void getMethodIDs(Nodes methodNodes, boolean isCpp)
	{
		for(int methodNum = 0; methodNum < methodNodes.size(); methodNum++)
		{
			SrcMethod srcMeth = new SrcMethod();
			Node methNode = methodNodes.get(methodNum);
			Nodes orphanMethName = methNode.query("./srcml:name", namespace);
			String mName = orphanMethName.get(0).getValue();
			
			
			if(isCpp)
				mName = cppFix(mName);
			
			System.out.println("Method Name: " + mName);
			srcMeth.name = mName;
			srcMeth.enclosingClassAttr = null;
			srcIdentifiers.methods.add(srcMeth);
			
			Nodes methAttributes = methNode.query(".//srcml:decl_stmt", namespace);
			getAttributeIDs(methAttributes, isCpp);
			
		}
	}
	
	//Extracts class identifiers from the class nodes
	//Also loops through and extracts the method and attribute nodes
	//found in the class
	public String getClassIDs(Nodes classNodes, boolean isCpp)
	{
		String result = new String();
		for(int classNum = 0; classNum < classNodes.size(); classNum++)
		{
			Node classNode = classNodes.get(classNum);
			final Nodes nameOfClass = classNode.query("./srcml:name", namespace);
			
			SrcClass classObj = new SrcClass();
			result = result + "\nClass Name: " + nameOfClass.get(0).getValue();
			String cName = nameOfClass.get(0).getValue();
			classObj.name = cName;
			Vector<SrcMethod> CMNames = new Vector<SrcMethod>();
			Vector<SrcAttribute> CANames = new Vector<SrcAttribute>();
			//System.out.println("Class Name: " + nameOfClass.get(0).getValue());
			
						
			//Find Orphan Attr in class
			Nodes classAttrNames = classNode.query("./srcml:block/srcml:decl_stmt/srcml:decl/srcml:name", namespace);
			Nodes classAttrTypes = classNode.query("./srcml:block/srcml:decl_stmt/srcml:decl/srcml:type", namespace);
			
			//Holds encling class attributes (used later)
			Vector<String> encClassAttr = new Vector<String>();
			for(int classAttrNum = 0; classAttrNum < classAttrNames.size(); classAttrNum++)
			{
				SrcAttribute attr = new SrcAttribute();
				result = result + "\n\t\tAttributes In Class: " + classAttrNames.get(classAttrNum).getValue() + " type: " + classAttrTypes.get(classAttrNum).getValue();
				
				encClassAttr.add(classAttrNames.get(classAttrNum).getValue());
				attr.name = classAttrNames.get(classAttrNum).getValue();
				attr.type = classAttrTypes.get(classAttrNum).getValue();
				
				CANames.add(attr);
				srcIdentifiers.attributes.add(attr);
				//System.out.println("\t\tAttributes In Class: " + classAttrs.get(classAttrNum).getValue());
			}
			
			Nodes methods = classNode.query(".//srcml:function | .//srcml:function_decl", namespace);
			for(int methNum = 0; methNum < methods.size(); methNum++)
			{
				boolean goAhead = false;
				
				
				Element method = (Element) methods.get(methNum);
				goAhead = checkIfDirectParentTheOneWeWant(nameOfClass.get(0).getValue(), method);
				if(goAhead)
				{
					SrcMethod srcMeth = new SrcMethod();
					Nodes classMethName = method.query("./srcml:name", namespace);
					String mName = classMethName.get(0).getValue();
					
					
					if(isCpp)
						mName = cppFix(mName);
					result = result + "\n\tClass Method Name: " + mName;
					
					srcMeth.name = mName;
					//Add enclosing class attributes
					srcMeth.enclosingClassAttr = encClassAttr;
					
					CMNames.add(srcMeth);
					srcIdentifiers.methods.add(srcMeth);
					//System.out.println("\tClass Method Name: " + classMethName.get(0).getValue());
					
					
					Nodes classMethAttributes = method.query(".//srcml:decl_stmt", namespace);
					for(int classMethAttrNum = 0; classMethAttrNum < classMethAttributes.size(); classMethAttrNum++)
					{
						//Make sure attribute is in method searched only
						boolean goAhead2 = false;
						Element classMethAttr = (Element)classMethAttributes.get(classMethAttrNum);
						goAhead2 = checkIfDirectParentTheOneWeWant(nameOfClass.get(0).getValue(), classMethAttr);
						
						if(goAhead2)
						{
							SrcAttribute attr = new SrcAttribute();
							
							Nodes nameOfClassMethAttr = classMethAttr.query("./srcml:decl/srcml:name", namespace);
							Nodes typeOfClassMethAttr = classMethAttr.query("./srcml:decl/srcml:type", namespace);
							result = result + "\n\t\tName of Attr: " + nameOfClassMethAttr.get(0).getValue();
							attr.name = nameOfClassMethAttr.get(0).getValue();
							attr.type = typeOfClassMethAttr.get(0).getValue();
							
							srcIdentifiers.attributes.add(attr);
							//System.out.println("\t\tName of Attr: " + nameOfClassMethAttr.get(0).getValue());
						}
					}
				}
			}
			classObj.classAttributes = CANames;
			classObj.classMethods = CMNames;
			srcIdentifiers.classes.add(classObj);
			
		}
		return result;
	}
	
	//Makes sure that parent nodes are the one we want
	private static boolean checkIfDirectParentTheOneWeWant(
		    final String nameOfClass, final Element method) {
		boolean goahead = false;
		if (nameOfClass != null) {
		    final Nodes parentClasses = method.query("ancestor::srcml:class",
			    namespace);

		    
		    //System.out.println("Direct parent class: " + parentClasses.get(parentClasses.size() - 1).query(".//srcml:name", namespace).get(0).getValue());
		    if (parentClasses.get(parentClasses.size() - 1)
			    .query(".//srcml:name", namespace).get(0)
			    .getValue().equalsIgnoreCase(nameOfClass)) {
			// System.out.println("Same parent class:" + nameOfClass);
			goahead = true;
		    }
		} else {
		    goahead = true;
		}
		return goahead;
	    }
	
	//This method splits the identifier for Cpp files
	public String cppFix(String name)
	{
		String result = name;
		String[] split =  name.split("::");
		
		if(split.length > 1)
		{
			System.out.println("split: " + split[1]);
			result = split[1];
		}
		
		return result;
	}
	
	
}



















































