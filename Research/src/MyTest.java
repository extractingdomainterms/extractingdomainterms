import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.junit.Assert;
import org.junit.Test;

import gate.*;
import gate.gui.MainFrame;
import gate.util.GateException;

public class MyTest {

    // Will Add to once methods are more complete
	// Test cases for various methods
    @Test
    public void testExtractJavaIDs() {

	final srcMLParser parser = new srcMLParser();
	final Builder builder = new Builder();
	final File xmlFile = new File("./Resources/output/test.java.xml");
	Document doc = null;
	try {
	    doc = builder.build(xmlFile);
	} catch (final ValidityException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (final ParsingException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (final IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	Assert.assertEquals("Number of oprhan attributes should be 1", 1,
		parser.getOrphanAttributes(doc).size());
	Assert.assertEquals("Number of orphan methods should be 1", 1, parser
		.getOrphanMethods(doc).size());
	Assert.assertEquals("Number Of Classes Should be 3", 3, parser
		.getClassNodes(doc).size());
	
	
    }

    @Test
    public void testExtractCIDs() {
	final srcMLParser parser = new srcMLParser();
	final Builder builder = new Builder();
	final File xmlFile = new File("./Resources/output/test2.c.xml");
	Document doc = null;
	try {
	    doc = builder.build(xmlFile);
	} catch (final ValidityException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (final ParsingException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (final IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	Assert.assertEquals("Number of oprhan attributes should be 2", 2,
		parser.getOrphanAttributes(doc).size());
	Assert.assertEquals("Number of orphan methods should be 2", 2, parser
		.getOrphanMethods(doc).size());
    }

    @Test
    public void testExtractCppIDs() {
	final srcMLParser parser = new srcMLParser();
	final Builder builder = new Builder();
	final File xmlFile = new File(
		"./Resources/output/asynchostresolver.cpp.xml");
	Document doc = null;
	try {
	    doc = builder.build(xmlFile);
	} catch (final ValidityException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (final ParsingException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (final IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	Assert.assertEquals("Number of oprhan attributes should be 1", 1,
		parser.getOrphanAttributes(doc).size());
	Assert.assertEquals("Number of orphan methods should be 7", 7, parser
		.getOrphanMethods(doc).size());
    }

    
    @Test
    public void testTermListCreator() {
    	ArrayList<String> result = new ArrayList<String>();
	
    	TermListCreator snakeTest1 = new TermListCreator("hello_test");
    	TermListCreator snakeTest2 = new TermListCreator("hi_test_again");
    	TermListCreator snakeTest3 = new TermListCreator("nosplit");
    	TermListCreator snakeTest4 = new TermListCreator("last_");
    	
    	//SnakeCase Tests
    	result = snakeTest1.splitIdentifier();
    	Assert.assertEquals("SnakeTest1 - 0 Should be: hello", "hello", result.get(0));
    	Assert.assertEquals("SnakeTest1 - 1 Should be: test", "test", result.get(1));
    	
    	result.clear();
    	result = snakeTest2.splitIdentifier();
    	Assert.assertEquals("SnakeTest2 - 0 Should be: hi", "hi", result.get(0));
    	Assert.assertEquals("SnakeTest2 - 1 Should be: test", "test", result.get(1));
    	Assert.assertEquals("SnakeTest2 - 2 Should be: again", "again", result.get(2));
    	
    	result.clear();
    	result = snakeTest3.splitIdentifier();
    	Assert.assertEquals("SnakeTest3 - 0 Should be: nosplit", "nosplit", result.get(0));
    	
    	result.clear();
    	result = snakeTest4.splitIdentifier();
    	Assert.assertEquals("SnakeTest4 - 0 Should be: last", "last", result.get(0));
    	
    	//CamelCase Tests
    	TermListCreator camelTest1 = new TermListCreator("TestCase");
    	TermListCreator camelTest2 = new TermListCreator("thisTestCase");
    	TermListCreator camelTest3 = new TermListCreator("nosplit");
    	TermListCreator camelTest4 = new TermListCreator("lasT");
    	
    	result = camelTest1.splitIdentifier();
    	Assert.assertEquals("CamelTest1 - 0 Should be: Test", "Test", result.get(0));
    	Assert.assertEquals("CamelTest1 - 1 Should be: Case", "Case", result.get(1));
    	
    	result.clear();
    	result = camelTest2.splitIdentifier();
    	Assert.assertEquals("CamelTest2 - 0 Should be: this", "this", result.get(0));
    	Assert.assertEquals("CamelTest2 - 1 Should be: Test", "Test", result.get(1));
    	Assert.assertEquals("CamelTest2 - 2 Should be: Case", "Case", result.get(2));
    	
    	result.clear();
    	result = camelTest3.splitIdentifier();
    	Assert.assertEquals("CamelTest3 - 0 Should be: nosplit", "nosplit", result.get(0));
    	
    	result.clear();
    	result = camelTest4.splitIdentifier();
    	Assert.assertEquals("CamelTest4 - 0 Should be: lasT", "las", result.get(0));
    	Assert.assertEquals("CamelTest4 - 1 Should be: T", "T", result.get(1));
    }
    
    @Test
    public void splitterTest() {
    	Splitter splitter = new Splitter();
    	Vector<String> result = new Vector<String>();
    	
    	result = splitter.splitIdentifier("str_Split_Test");
    	System.out.println("splitter: " + result);
    	Assert.assertEquals("Splitter - 0 Should be: str", "str", result.get(0));
    	Assert.assertEquals("Splitter - 1 Should be: split", "split", result.get(1));
    	Assert.assertEquals("Splitter - 2 Should be: test", "test", result.get(2));
    
    	
    	//Remove Hungarian Test
    	result = splitter.removeHungarianNotation(result);
    	System.out.println("s: " + result);
    	Assert.assertEquals("Splitter - 0 Should be: split", "split", result.get(0));
    	Assert.assertEquals("Splitter - 1 Should be: test", "test", result.get(1));
    	
    	result = splitter.splitIdentifier("iSplitTest");
    	result = splitter.removeHungarianNotation(result);
    	System.out.println("s: " + result);
    	Assert.assertEquals("Splitter - 0 Should be: split", "split", result.get(0));
    	Assert.assertEquals("Splitter - 1 Should be: test", "test", result.get(1));
    }
    
    
    @Test
    public void ExtractPOSTest()
    {
    	ExtractRelations ER = new ExtractRelations();
    	ER.buildOntology("Hello, this is a sentence.  I do want to extract specific noun phrases, such as remote file");
    	System.out.println("ersize: " + ER.concepts.size() + " " + ER.verbRelations.size());
    	Assert.assertEquals("First concept should be 'sentence'", "sentence", ER.concepts.get(0));
    	Assert.assertEquals("First verb should be 'is'", "is", ER.verbRelations.get(0));
    }
}
