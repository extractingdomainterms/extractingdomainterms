import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;

public class Splitter {

	//This method splits the identifiers in various ways, such as CamelCase, snake_case, etc...
	public static Vector<String> splitIdentifier(final String identifier) {
		String[] tempSplitedOnSpecialCharsAndDigits = null;
		String[] tempSplitedOnLowercaseToUppercase = null;
		String[] tempSplitedOnOnUppercaseToLowercase = null;
		final Vector<String> splitedTerms = new Vector<String>();
		tempSplitedOnSpecialCharsAndDigits = Splitter.splitOnSpecialCharsAndDigits(identifier);
		for (final String currentTermNoSpecChars : tempSplitedOnSpecialCharsAndDigits) {
			tempSplitedOnLowercaseToUppercase = Splitter.splitOnLowercaseToUppercase(currentTermNoSpecChars);
			for (final String currentSplittedOnLowerToUpper : tempSplitedOnLowercaseToUppercase) {
				tempSplitedOnOnUppercaseToLowercase = Splitter.splitOnUppercaseToLowercase(currentSplittedOnLowerToUpper);
				for (final String currentSplittedOnUpperToLower : tempSplitedOnOnUppercaseToLowercase) {
					if (currentSplittedOnUpperToLower.length() > 0) {
						//System.out.println("\t\t will add " + currentSplittedOnUpperToLower);
						splitedTerms.add(currentSplittedOnUpperToLower.toLowerCase());
					}
				}
			}
		}
		splitedTerms.trimToSize();
		return splitedTerms;
	}

	public static String[] splitOnSpecialCharsAndDigits(final String token) {
		final Pattern patternSpecialCharsAndDigits =
			Pattern.compile(" |\\n|\\r|\\t|\\.|,|\\(|\\)|\\{|\\}|\\[|\\]|;|=|<|>|'|\""
					+ "|\\+|\\-|\\*|/|\\||!|\\\\|\\@|#|\\$|%|\\?|&|\\^|_|:|\\||~|`|�|�|[0-9]+");
		return patternSpecialCharsAndDigits.split(token);
	}

	private static String[] splitOnLowercaseToUppercase(final String token) {
		String[] result = null;
		final Pattern patternLowercaseToUppercase = Pattern.compile("[A-Z]+");
		final Matcher action = patternLowercaseToUppercase.matcher(token);
		result = action.replaceAll(" " + "$0").split(" ");
		//		result = action.replaceAll(" " + "$0").toLowerCase().split(" ");
		return result;
	}

	private static String[] splitOnUppercaseToLowercase(final String token) {
		String[] result = null;
		final Pattern patternUppercaseToLowercas = Pattern.compile("[A-Z][a-z]+");
		final Matcher action = patternUppercaseToLowercas.matcher(token);
		result = action.replaceAll(" " + "$0").split(" ");
		return result;
	}

	public static Vector<String> getIdentifierForTag(final String tag, final String comment) {
		final Vector<String> result = new Vector<String>();

		final Pattern patternSpecialCharsAndDigits =
			Pattern.compile(" |\\n|\\r|\\t|\\.|,|\\(|\\)|\\{|\\}|\\[|\\]|;|=|<|>|'|\""
					+ "|\\+|\\-|\\*|/|\\||!|\\\\|#|\\$|%|\\?|&|\\^|:|\\||~|`|�|�");
		final String[] commentSplit = patternSpecialCharsAndDigits.split(comment);

		int i = 0;
		while (i < commentSplit.length) {
			if (commentSplit[i].equalsIgnoreCase("@param") && i + 1 < commentSplit.length) {
				result.add(commentSplit[i + 1]);
				i = i + 2;
			}
			else {
				i++;
			}

		}
		return result;

	}
	
	//This method removes Hungarian notation from the identifiers extracted previously
	public Vector<String> removeHungarianNotation(Vector<String> identifiers)
	{
		final String[] hungarianNotations = {"b", "c", "str", "si", "i", "li", "f", 
				"d", "ld", "sz", "os", "s", "c", "u", "k", "r", "s", "rg", "p", "prg"};
		
		
		if(ArrayUtils.contains(hungarianNotations, identifiers.get(0)))
		{
			identifiers.remove(0);
		}
	
		
		return identifiers;
		
	}

}
