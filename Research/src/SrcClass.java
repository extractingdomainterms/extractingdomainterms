import java.util.Vector;

public class SrcClass {
	public String name;
	public Vector<SrcMethod> classMethods = new Vector<SrcMethod>();
	public Vector<SrcAttribute> classAttributes = new Vector<SrcAttribute>();
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Vector<SrcMethod> getClassMethods() {
		return classMethods;
	}
	public void setClassMethods(Vector<SrcMethod> classMethods) {
		this.classMethods = classMethods;
	}
	public Vector<SrcAttribute> getClassAttributes() {
		return classAttributes;
	}
	public void setClassAttributes(Vector<SrcAttribute> classAttributes) {
		this.classAttributes = classAttributes;
	}
	
	
}
