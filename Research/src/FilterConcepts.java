
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import org.apache.lucene.*;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Fields;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class FilterConcepts {

	public static final String pathToDocuments = "/home/alex/workspace/Documents/FZDoc";
	public static final String pathToIndex = "/home/alex/workspace/index";
	
	public void indexDocuments()
	{
		File folder = new File(pathToDocuments);
		File[] listOfFiles = folder.listFiles();
				
		try{
			Directory dir = FSDirectory.open(Paths.get(pathToIndex));
			 
			Analyzer analyzer = new StandardAnalyzer();
			IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
			IndexWriter iw = new IndexWriter(dir, iwc);
			
			for(File file: listOfFiles)
			{
				InputStream stream = new FileInputStream(file);
			 
				Document document = new Document();
								
				Field pathField = new StringField("path", file.toString(), Field.Store.YES);
				document.add(pathField);
				document.add(new TextField("contents", new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))));
				
				iw.addDocument(document);
				System.out.println("Added " + file.toString());
			}
			iw.close();
			
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void searchIndex(String searchString)
	{
		try {
			Directory dir = FSDirectory.open(Paths.get(pathToIndex));
			IndexReader ireader = DirectoryReader.open(dir);
			IndexSearcher indexSearcher = new IndexSearcher(ireader);

			Fields fields = MultiFields.getFields(ireader);
			System.out.println("fields: " + fields.toString() + " size: " + fields.size());
			Terms terms = fields.terms("contents");
			Term t = new Term("contents");
			System.out.println("size is: " + fields.terms("contents").size());
			
			Analyzer analyzer = new StandardAnalyzer();
			
			QueryParser queryParser = new QueryParser("contents", analyzer);
			Query query = queryParser.parse(searchString);
			TopScoreDocCollector collector = TopScoreDocCollector.create(1000);
			indexSearcher.search(query, collector);
			
			ScoreDoc[] hits = collector.topDocs().scoreDocs;
			System.out.println("found " + hits.length + " hits");
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
