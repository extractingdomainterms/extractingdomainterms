import java.util.Vector;

public class SrcMethod {
	
	public String name;
	public Vector<String> enclosingClassAttr = new Vector<String>();
	
	public Vector<String> getEnclosingClassAttr() {
		return enclosingClassAttr;
	}
	public void setEnclosingClassAttr(Vector<String> enclosingClassAttr) {
		this.enclosingClassAttr = enclosingClassAttr;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	

}
