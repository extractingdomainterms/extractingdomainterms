import java.util.ArrayList;
import java.util.Vector;

import org.apache.commons.lang.ArrayUtils;

public class TermListCreator {
	
	public String identifier;
	ArrayList<String> termList = new ArrayList<String>();
	
	public TermListCreator(String identifier) {
		// TODO Auto-generated constructor stub
		this.identifier = identifier;
	}
	
	//This method originally was used to split identifiers, but was replaced by the "Splitter" class
	//which does the job better.  Both work, however the Splitter class has been tested more.
	public ArrayList<String> splitIdentifier()
	{
		boolean isCamelCase = false;
		boolean isSnakeCase = false; //Snake case uses underscore _ as seperator
		
		String IDCopy = this.identifier;  
		
		//Start at 1 to skip first character (_variable)
		for(int i = 1; i < IDCopy.length(); i++)  
		{
			//If character is a - or _
			if(IDCopy.charAt(i) == '_' || IDCopy.charAt(i) == '-')
			{
				isSnakeCase = true;
			}
			//If character is uppercase and character before is lowercase
			if((IDCopy.charAt(i) >= 'A' && IDCopy.charAt(i) <= 'Z')
					&& (IDCopy.charAt(i-1) >= 'a' && IDCopy.charAt(i-1) <= 'z'))
			{
				isCamelCase = true;
			}
		}
		
		//Now start splitting
		int startIndex = 0;
		int endIndex = 0;
		
		//If neither snakeCase or CamelCase, (i.e. variablename)
		if(!isSnakeCase && !isCamelCase)
		{
			termList.add(IDCopy);
		}
		
		
		//If snakeCase
		if(isSnakeCase)
		{
			for(int index = 0; index < IDCopy.length(); index++)
			{
				if(IDCopy.charAt(index) == '-' || IDCopy.charAt(index) == '_')
				{
					endIndex = index;
					//System.out.println("Snake - Start/End: " + startIndex + " " + endIndex);
					if(startIndex != endIndex)
						termList.add(IDCopy.substring(startIndex, endIndex));
					
					startIndex = index+1;
				}
				if(index == IDCopy.length()-1)
				{
					endIndex = IDCopy.length();
					//System.out.println("Snake - Start/End: " + startIndex + " " + endIndex);
					if(startIndex != endIndex)
						termList.add(IDCopy.substring(startIndex, endIndex));
				}
			}
		}
		
		//If CamelCase
		if(isCamelCase)
		{
			for(int index = 1; index < IDCopy.length(); index++)
			{
				if((IDCopy.charAt(index) >= 'A' && IDCopy.charAt(index) <= 'Z')
						&& (IDCopy.charAt(index-1) >= 'a' && IDCopy.charAt(index-1) <= 'z'))
				{
					endIndex = index;
					//System.out.println("Camel - Start/End: " + startIndex + " " + endIndex);
					if(startIndex != endIndex)
						termList.add(IDCopy.substring(startIndex, endIndex));
					
					startIndex = index;
				}
				if(index == IDCopy.length()-1)
				{
					endIndex = IDCopy.length();
					//System.out.println("Camel2 - Start/End: " + startIndex + " " + endIndex);
					if(startIndex != endIndex)
						termList.add(IDCopy.substring(startIndex, endIndex));
				}
			}
		}
		
		return termList;
	}
	
	
	
	//public String removeHungarianNotation()
	//{
		
		
	//}

}
