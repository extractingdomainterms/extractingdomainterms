import java.util.*;

public class SourceIdentifiers {

	public String fileName;
	public Vector<SrcAttribute> attributes = new Vector<SrcAttribute>();
	public Vector<SrcMethod> methods = new Vector<SrcMethod>();  //Can get mixed in!
	public Vector<SrcClass> classes = new Vector<SrcClass>();
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public List<SrcAttribute> getAttributes() {
		return attributes;
	}
	public void setAttributes(Vector<SrcAttribute> attributes) {
		this.attributes = attributes;
	}
	public List<SrcMethod> getMethods() {
		return methods;
	}
	public void setMethods(Vector<SrcMethod> methods) {
		this.methods = methods;
	}
	public List<SrcClass> getClasses() {
		return classes;
	}
	public void setClasses(Vector<SrcClass> classes) {
		this.classes = classes;
	}
	public void print()
	{
		int numAttr = this.attributes.size();
		int numMeth = this.methods.size();
		int numClasses = this.classes.size();
		
		String printVal = "Orphan Attributes: ";
		
		for(int i = 0; i < numAttr; i++)
		{
			printVal += attributes.get(i).type + " " + attributes.get(i).name + " ";
		}
		
		printVal += "\nOrphan Methods: ";
		for(int x = 0; x < numMeth; x++)
		{
			printVal += methods.get(x).name + " " + methods.get(x).enclosingClassAttr + " ";
		}
		System.out.println(printVal);
		
		//printVal += "\nClasses: ";
		System.out.println("num classes: " + numClasses);
		for(int y = 0; y < numClasses; y++)
		{
			System.out.println("Name: " + classes.get(y).name);
			for(int w = 0; w < classes.get(y).classAttributes.size(); w++)
			{
				System.out.println("\tAttributes: " + classes.get(y).classAttributes.get(w).getType() + " " + classes.get(y).classAttributes.get(w).getName());
			}
			for(int z = 0; z < classes.get(y).classMethods.size(); z++)
			{
				System.out.println("\tMethods: " + classes.get(y).classMethods.get(z).name);
			}
		}
		
	}
	
	public void clear()
	{
		this.attributes.clear();
		this.methods.clear();
		this.classes.clear();
	}
	
	//Takes the identifiers found and splits them up by camelCase, snakeCase, etc...
	public void splitIDs()
	{
		int numAttr = this.attributes.size();
		int numMeth = this.methods.size();
		int numClasses = this.classes.size();
		
		Splitter splitter = new Splitter();
    	Vector<String> result = new Vector<String>();
    					
		for(int i = 0; i < numAttr; i++)
		{
			String attrName = "";
			result = splitter.splitIdentifier(this.attributes.get(i).name);
			
			for(int q = 0; q < result.size(); q++)
			{
				attrName += result.get(q) + " ";
			}
			this.attributes.get(i).name = attrName;
			result.clear();
		}
		
		for(int x = 0; x < numMeth; x++)
		{
			String methName = "";
			result = splitter.splitIdentifier(this.methods.get(x).name);
			
			for(int q = 0; q < result.size(); q++)
			{
				methName += result.get(q) + " ";
			}
			this.methods.get(x).name = methName;
			result.clear();
		}
		
		for(int y = 0; y < numClasses; y++)
		{
			String className = "";
			result = splitter.splitIdentifier(this.classes.get(y).name);
			
			for(int q = 0; q < result.size(); q++)
			{
				className += result.get(q) + " ";
			}
			this.classes.get(y).name = className;
			result.clear();
			
			//Parse class methods
			for(int j = 0; j < this.classes.get(y).getClassMethods().size(); j++)
			{
				String methodName = "";
				
				result = splitter.splitIdentifier(this.classes.get(y).getClassMethods().get(j).name);
				for(int u = 0; u < result.size(); u++)
				{
					methodName += result.get(u) + " "; 
				}
				this.classes.get(y).getClassMethods().get(j).name = methodName;
				result.clear();
			}
			
			//Parse class attributes
			for(int m = 0; m < this.classes.get(y).getClassAttributes().size(); m++)
			{
				String attributeName = "";
				
				result = splitter.splitIdentifier(this.classes.get(y).getClassAttributes().get(m).name);
				for(int v = 0; v < result.size(); v++)
				{
					attributeName += result.get(v) + " ";
				}
				this.classes.get(y).getClassAttributes().get(m).name = attributeName;
				result.clear();
			}
		}
	}
	
	
	
}
