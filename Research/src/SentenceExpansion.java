import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWordSet;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.dictionary.Dictionary;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations.CorefChainAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.parser.nndep.Configuration;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;
import edu.stanford.nlp.util.CoreMap;


public class SentenceExpansion {
	
	Dictionary dict = null;
	IndexWordSet iw = null;
	
	SentenceExpansion()
	{
		try {
			JWNL.initialize(new FileInputStream("/home/alex/Documents/ResearchTools/jwnl14-rc2/config/file_properties.xml"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JWNLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dict = Dictionary.getInstance();
	}
	
	//Takes sourceIdentifier and returns it's candidate sentences
	public Vector<String> generateCandidateSentences(SourceIdentifiers srcID)
	{
		Vector<String> aCandidateSentences = new Vector<String>();
		Vector<String> mCandidateSentences = new Vector<String>();
		Vector<String> cCandidateSentences = new Vector<String>();
		Vector<String> cmCandidateSentences = new Vector<String>();
		Vector<String> caCandidateSentences = new Vector<String>();
		
		ExtractRelations ER = new ExtractRelations();
		
		
		for(int i = 0; i < srcID.getAttributes().size(); i++)
		{
			boolean isBool = false;
			if(srcID.getAttributes().get(i).type.equals("boolean"))
				isBool = true;
			
			Vector<String> attrInput = stringToVect(srcID.getAttributes().get(i).name);
			aCandidateSentences.add(createCandidateSentencesForAttributes(attrInput, isBool));
			attrInput.clear();
		}
		
		for(int x = 0; x < srcID.getMethods().size(); x++)
		{
			Vector<String> methInput = stringToVect(srcID.getMethods().get(x).name);
			mCandidateSentences.add(createCandidateSentencesForMethods(methInput, srcID.getMethods().get(x).enclosingClassAttr));
			methInput.clear();
		}
		
		for(int y = 0; y < srcID.getClasses().size(); y++)
		{
			String enclosingClassName = srcID.getClasses().get(y).name;
			
			Vector<String> className = stringToVect(srcID.getClasses().get(y).name);
			cCandidateSentences.add(createCandidateSentencesForClasses(className));
			for(int w = 0; w < srcID.classes.get(y).classAttributes.size(); w++)
			{
				boolean isBool = false;
				if(srcID.getClasses().get(y).getClassAttributes().get(w).type.equals("boolean"))
					isBool = true;
				
				Vector<String> classAttrInput = stringToVect(srcID.getClasses().get(y).getClassAttributes().get(w).name);
				String canSentence = createCandidateSentencesForAttributes(classAttrInput, isBool);
				canSentence = replaceSubjectWithClassName(canSentence, enclosingClassName);
				caCandidateSentences.add(canSentence);
				classAttrInput.clear();
			}
			for(int z = 0; z < srcID.classes.get(y).classMethods.size(); z++)
			{
				Vector<String> classMethInput = stringToVect(srcID.getClasses().get(y).getClassMethods().get(z).name);
				String canSentence2 = createCandidateSentencesForMethods(classMethInput, srcID.getClasses().get(y).getClassMethods().get(z).enclosingClassAttr);
				canSentence2 = replaceSubjectWithClassName(canSentence2, enclosingClassName);
				cmCandidateSentences.add(canSentence2);
				classMethInput.clear();
			}
			className.clear();
		}
		
		//temp fix
		System.out.println("******************************");
		System.out.println("Attribute Sentences: " + aCandidateSentences);
		System.out.println("Method Sentences: " + mCandidateSentences);
		System.out.println("Class Sentences: " + cCandidateSentences);
		System.out.println("Class Attr Sentences: " + caCandidateSentences);
		System.out.println("Class Method sentences: " + cmCandidateSentences);
		System.out.println("******************************");
		
		Vector<String> allCandidateSentences = new Vector<String>();
		allCandidateSentences.addAll(aCandidateSentences);
		allCandidateSentences.addAll(mCandidateSentences);
		allCandidateSentences.addAll(cCandidateSentences);
		allCandidateSentences.addAll(caCandidateSentences);
		allCandidateSentences.addAll(cmCandidateSentences);
		
		return allCandidateSentences;
	}
	
	//As per one of the rules described in the paper,
	//this method replaces "subject" in a candidate sentence with the name of
	//the enclosing class.  
	public String replaceSubjectWithClassName(String inputSentence, String className)
	{
		String ret = inputSentence.replace("subject ", className);
		String ret2 = ret.replace("subjects ", className);
		return ret2;
	}
	
	//Takes a string and converts it to a vector of single words
	public Vector<String> stringToVect(String inputStr)
	{
		String[] split =  inputStr.split(" ");
		
		Vector<String> ret = new Vector<String>(Arrays.asList(split));
		return ret;
	}

	//Creates candidate sentences for class identifiers
	public String createCandidateSentencesForClasses(final Vector<String> termList)
	{
		
		try {
			iw = dict.lookupAllIndexWords(termList.get(0));
		} catch (JWNLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String candidateSentenceN = null;
		String candidateSentenceV = null;
		
		if (iw.getIndexWord(POS.NOUN) != null) {
			if(termList.size() == 1)
			{
				//Apply CR1
				candidateSentenceN = applyCR1(termList);
			}
			else
			{
				//Apply CR3
				candidateSentenceN = applyCR3(termList);
			}
			
		}
		if (iw.getIndexWord(POS.VERB) != null) {
			if(termList.size() == 1)
			{
				//Apply CR2
				candidateSentenceV = applyCR2(termList);
			}
			else
			{
				//Apply CR4
				candidateSentenceV = applyCR4(termList);
			}
			
		}
		

		//Now we need to see if there are two possible candidate sentences, and if so, select one
		if(candidateSentenceN != null && candidateSentenceV != null)
		{	
			//1.)  If T1 as a verb freq > T1 as a noun, select candidateSentenceV (and vise-versa)
			//2.)  If equal freq, choose candidateSentenceN because terms are from a class
			
			
			//1.)
			if(iw.getSenseCount(POS.NOUN) > iw.getSenseCount(POS.VERB))
			{
				return candidateSentenceN;
			}
			if(iw.getSenseCount(POS.NOUN) < iw.getSenseCount(POS.VERB))
			{
				return candidateSentenceV;
			}
			
			//2.)
			return candidateSentenceN;
		}
		else if(candidateSentenceN != null && candidateSentenceV == null)
		{
			return candidateSentenceN;
		}
		else if(candidateSentenceN == null && candidateSentenceV != null)
		{
			return candidateSentenceV;
		}
		
		if(termList.size() == 1)
		{
			//Apply CR1
			candidateSentenceN = applyCR1(termList);
		}
		else
		{
			//Apply CR3
			candidateSentenceN = applyCR3(termList);
		}
		return candidateSentenceN;
	}
	
	//Creates candidate sentences from method identifiers
	public String createCandidateSentencesForMethods(final Vector<String> termList, final Vector<String> enclosingClassAttributes)
	{
		
		try {
			iw = dict.lookupAllIndexWords(termList.get(0));
		} catch (JWNLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String candidateSentenceN = null;
		String candidateSentenceV = null;
		if (iw.getIndexWord(POS.NOUN) != null) {
			if(termList.size() == 1)
			{
				//Apply MR2
				candidateSentenceN = applyMR2(termList);
			}
			else
			{
				//Apply MR4
				candidateSentenceN = applyMR4(termList);
			}
			
		}
		if (iw.getIndexWord(POS.VERB) != null) {
			if(termList.size() == 1)
			{
				//Apply MR1
				//System.out.println("applying mr1");
				candidateSentenceV = applyMR1(termList);
			}
			else
			{
				//Apply MR3
				//System.out.println("Applying mr3");
				candidateSentenceV = applyMR3(termList);
			}
			
		}
		
		//Now we need to see if there are two possible candidate sentences, and if so, select one
		if(candidateSentenceN != null && candidateSentenceV != null)
		{
			//1.)  If T1, T2, ... is an attribute, select candidateSentenceN			
			//2.)  If T1 as a verb freq > T1 as a noun, select candidateSentenceV (and vise-versa)
			//3.)  If equal freq, choose candidateSentenceV because terms are from a method
			
			//1.)
			//Check if verb sentence is "get T1..."
			if(enclosingClassAttributes != null)
			{
				if(termList.get(0).equals("get"))
				{
					String getVar = "";
					for(int i = 1; i < termList.size(); i++)
					{
						getVar += getVar + termList.get(i);
					}
					if(enclosingClassAttributes.contains(getVar))
					{
						return candidateSentenceV;
					}
					else
					{
						if(iw.getSenseCount(POS.NOUN) > iw.getSenseCount(POS.VERB))
						{
							return candidateSentenceN;
						}
						if(iw.getSenseCount(POS.NOUN) < iw.getSenseCount(POS.VERB))
						{
							return candidateSentenceV;
						}
					}
				}
				//If not "get T1..." then see if any terms are attributes
				for(int i = 0; i < termList.size(); i++)
				{
					if(enclosingClassAttributes.contains(termList.get(i)))
					{
						return candidateSentenceN;
					}
				}
			}
			//2.)
			if(iw.getSenseCount(POS.NOUN) > iw.getSenseCount(POS.VERB))
			{
				return candidateSentenceN;
			}
			if(iw.getSenseCount(POS.NOUN) < iw.getSenseCount(POS.VERB))
			{
				return candidateSentenceV;
			}
			
			//3.)
			return candidateSentenceV;
		}
		else if(candidateSentenceN != null && candidateSentenceV == null)
		{
			return candidateSentenceN;
		}
		else if(candidateSentenceN == null && candidateSentenceV != null)
		{
			return candidateSentenceV;
		}
		
		if(termList.size() == 1)
		{
			//Apply MR1
			//System.out.println("applying mr1");
			candidateSentenceV = applyMR1(termList);
		}
		else
		{
			//Apply MR3
			//System.out.println("Applying mr3");
			candidateSentenceV = applyMR3(termList);
		}
		return candidateSentenceV;
	}
	
	//Creates candidate sentences from attribute identifiers
	public String createCandidateSentencesForAttributes(final Vector<String> termList, boolean isBoolType)
	{
		
		try {
			iw = dict.lookupAllIndexWords(termList.get(0));
		} catch (JWNLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String candidateSentenceN = null;
		String candidateSentenceV = null;
		if (iw.getIndexWord(POS.NOUN) != null) {
			if(termList.size() == 1)
			{
				//Apply AR1
				candidateSentenceN = applyAR1(termList);
			}
			else
			{
				//Apply AR4
				candidateSentenceN = applyAR4(termList);
			}
			
		}
		if (iw.getIndexWord(POS.VERB) != null) {
			if(termList.size() == 1)
			{
				//Could be AR2 or AR3
				//AR2 - !PTV or (PTV and !bool type)
				//AR3 - PTV and bool type
				
				//Check if AR3
				if(isAPastTenseVerb(termList.get(0)) && isBoolType)
				{
					//System.out.println("PST and bool");
					candidateSentenceV = applyAR3(termList);
				}
				else if(!isAPastTenseVerb(termList.get(0)) || (isAPastTenseVerb(termList.get(0)) && !isBoolType))
				{
					//System.out.println("!PST or (PST and !bool)");
					candidateSentenceV = applyAR2(termList);
				}
				else
				{
					//System.out.println("hmm.  something went wroing\n\n");
				}
			}
			else
			{
				//Apply AR5
				candidateSentenceV = applyAR5(termList);
			}
			
		}
		
		//Now we need to see if there are two possible candidate sentences, and if so, select one
		if(candidateSentenceN != null && candidateSentenceV != null)
		{	
			//1.)  If T1 as a verb freq > T1 as a noun, select candidateSentenceV (and vise-versa)
			//2.)  If equal freq, choose candidateSentenceN because terms are from an attribute
			
			
			//1.)
			if(iw.getSenseCount(POS.NOUN) > iw.getSenseCount(POS.VERB))
			{
				return candidateSentenceN;
			}
			if(iw.getSenseCount(POS.NOUN) < iw.getSenseCount(POS.VERB))
			{
				return candidateSentenceV;
			}
			
			//2.)
			return candidateSentenceN;
		}
		else if(candidateSentenceN != null && candidateSentenceV == null)
		{
			return candidateSentenceN;
		}
		else if(candidateSentenceN == null && candidateSentenceV != null)
		{
			return candidateSentenceV;
		}
		
		if(termList.size() == 1)
		{
			//Apply AR1
			candidateSentenceN = applyAR1(termList);
		}
		else
		{
			//Apply AR4
			candidateSentenceN = applyAR4(termList);
		}
		return candidateSentenceN;
	}
	
	//As per one of the constraints described in "Natural Language Parsing of Program 
	//Element Names for Concept Extraction", this method checks if a given verb is past tense
	public boolean isAPastTenseVerb(String term)
	{
		boolean isPastTenseVerb = false;
		
		String partOfSpeech = null;
		
		srcMLParser parser = new srcMLParser();
		ExtractRelations ER = new ExtractRelations();
		
		//get the part of speech
		partOfSpeech = ER.buildOntology(term);
		//System.out.println("part of Speech: " + partOfSpeech);
		
		if(partOfSpeech.equals("VBD") || partOfSpeech.equals("VBN"))
			isPastTenseVerb = true;

		return isPastTenseVerb;
	}
	
	public String applyCR1(final Vector<String> termList)
	{
		return termList.get(0) + " is a thing";
	}
	
	public String applyCR2(final Vector<String> termList)
	{
		return termList.get(0) + "er is a thing";
	}
	
	public String applyCR3(final Vector<String> termList)
	{
		String begin = "";
		int i = 0;
		while(i < termList.size())
		{
			begin = begin + termList.get(i) + " ";
			i++;
		}
		return begin + "is a thing";
	}
	
	public String applyCR4(final Vector<String> termList)
	{
		String begin = "";
		int i = 1;
		while(i < termList.size())
		{
			begin = begin + termList.get(i) + " ";
			i++;
		}
		return termList.get(0) + "ing " + begin + "is a thing";
	}
	
	public String applyMR1(final Vector<String> termList)
	{
		return "subjects " + termList.get(0) + " object";
	}
	
	public String applyMR2(final Vector<String> termList)
	{
		return "subjects get " + termList.get(0);
	}
	
	public String applyMR3(final Vector<String> termList)
	{
		String begin = "";
		int i = 0;
		while(i < termList.size())
		{
			begin = begin + termList.get(i) + " ";
			i++;
		}
		return "subjects " + begin;
	}
	
	public String applyMR4(final Vector<String> termList)
	{
		String begin = "";
		int i = 0;
		while(i < termList.size())
		{
			begin = begin + termList.get(i) + " ";
			i++;
		}
		return "subjects get " + begin;
	}
	
	public String applyAR1(final Vector<String> termList)
	{
		//System.out.println("doing AR1");
		return termList.get(0) + " is a thing";
	}
	
	public String applyAR2(final Vector<String> termList)
	{
		//System.out.println("doing AR2");
		return termList.get(0) + "er is a thing";
	}
	
	public String applyAR3(final Vector<String> termList)
	{
		//System.out.println("doing AR3");
		return termList.get(0) + " subjects are things";
	}
	
	public String applyAR4(final Vector<String> termList)
	{
		//System.out.println("doing AR4");
		String begin = "";
		int i = 0;
		while(i < termList.size())
		{
			begin = begin + termList.get(i) + " ";
			i++;
		}
		return begin + "is a thing";
	}
	
	public String applyAR5(final Vector<String> termList)
	{
		//System.out.println("doing AR5");
		String begin = "";
		int i = 1;
		while(i < termList.size())
		{
			begin = begin + termList.get(i) + " ";
			i++;
		}
		return termList.get(0) + "ing " + begin + "is a thing";
	}
}
	
