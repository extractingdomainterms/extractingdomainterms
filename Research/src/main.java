import java.io.IOException;
import java.util.*;
import gate.*;
import gate.gui.MainFrame;
import gate.util.GateException;

public class main {

	public static void main(String[] args) throws GateException {
		
		List<String> fileList = new ArrayList<String>();
		srcMLParser parser = new srcMLParser();
		ExtractRelations ER = new ExtractRelations();
		SourceIdentifiers srcIDs = new SourceIdentifiers();
    	SentenceExpansion se = new SentenceExpansion();
    	
    	//Get the list of XML files.  The XML files were created using srcML and ran on Winmerge and Filezilla.
    	//Right it just populates the file list for one project (other file destination is commented out in the method).
    	//So, it just does it for Winmerge or Filezilla depending on what is commented out...
		fileList = parser.populateXMLFileList();
		
		//Go through each file, one at a time, in fileList
		for(int fileNum = 0; fileNum < fileList.size(); fileNum++)
		{
			srcIDs.clear();
			
			//Extract the identifiers from the XML file
			srcIDs = parser.extractClassesAndTheirMethods(fileList.get(fileNum));
			
			//Print out the identifiers, just to make sure it looks correct
			srcIDs.print();
			System.out.println("--------------------------------------------------");
			
			//Split each identifier extracted (Classes, methods, attributes).  For example, CarCourse -> car course
			srcIDs.splitIDs();
			srcIDs.print();
			System.out.println("--------------------------------------------------");
			
			//Generate candidate sentences (as per "Natural Language Parsing of Program Element
			//Names for Concept Extraction" paper) for each identifier extracted above.  Returns the candidate sentences in a vector
			//of strings
			Vector<String> candidateSentences = new Vector<String>(se.generateCandidateSentences(srcIDs));
			
			//Go through each candidate sentence and tryo to build an ontology from it.
			//Currently it just looks at noun, noun phrases, and verbs and tries to organize them
			//in a somewhat readable format.  It also applies some rules from the "Extract concepts and relations"
			//section of the research papers.  Such as, if the verb is "get" or "set", change it to "has property".
			for(String s: candidateSentences)
			{
				String t = ER.buildOntology(s);
			}
			
		}

		//This is the last section which deals with filtering the ontology extracted.  It uses Lucene to build
		//an index from multiple documents found online and in the documentation.  It is then supposed to filter out
		//words that aren't in the top 100 weighted words. The documents are found in .../Resources/Documentation/
		/*
		FilterConcepts f = new FilterConcepts();
		f.indexDocuments();
		f.searchIndex("ftp");
		*/
	}

}
