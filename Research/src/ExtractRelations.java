
import java.io.*;
import java.util.*;

import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.IndexWordSet;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.data.PointerType;
import net.didion.jwnl.data.PointerUtils;
import net.didion.jwnl.data.Synset;
import net.didion.jwnl.data.Word;
import net.didion.jwnl.data.list.PointerTargetNode;
import net.didion.jwnl.data.list.PointerTargetNodeList;
import net.didion.jwnl.data.list.PointerTargetTree;
import net.didion.jwnl.data.relationship.RelationshipFinder;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations.CorefChainAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.CoreNLPProtos.CorefChain;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.util.CoreMap;


public class ExtractRelations {

	
	
	//Concepts = Noun and noun phrases
	Vector<String> concepts = new Vector<String>();
	Vector<String> verbRelations = new Vector<String>();
	Vector<String> nounPhrases = new Vector<String>();
		
    private static StanfordCoreNLP pipeline;
    private static LexicalizedParser lp;
    private static TreebankLanguagePack tlp;
    private static GrammaticalStructureFactory gsf;
    private static TokenizerFactory<CoreLabel> tokenizerFactory;
    private static Properties props;
		
    
    //This method takes a candidate sentence and extracts the part of speech of various parts and helps build
    //an ontology from it.  Needs work to clean up and build a true ontology.
    //SentenceExtraction.isAPastTenseVerb() uses this method to determine the part of speech of a single word.
    //Can be edited later to have a seperate method do that instead.
	public String buildOntology(String input)
	{
		//Return POS string
		String partOfSpeech = "";
		
		//Ontology string is just a visual printout representation of an ontology extracted from the
		//candidate sentence that was given.  It's a little rough around the edges...
		String ontologyString = "";
		
		//Create StandfordCoreNLP object
		ExtractRelations.props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");	
		ExtractRelations.lp = LexicalizedParser
				.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");
		ExtractRelations.pipeline = new StanfordCoreNLP(props);
		ExtractRelations.tokenizerFactory = PTBTokenizer.factory(
				new CoreLabelTokenFactory(), "");
		
		Annotation document = new Annotation(input);
		
		//Run annotators on text
		pipeline.annotate(document);
		
		//Create CoreMap
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		
		//Extract Noun phrases
		Tree sentenceTree = getTree(input);
		System.out.println("sentence tree: " + sentenceTree);
		TregexPattern NPpattern = TregexPattern.compile("@NP");
		TregexMatcher matcher = NPpattern.matcher(sentenceTree);
		String nounInPhrase = "";
		Vector<String> nounsInPhrase = new Vector<String>();
		
		while (matcher.findNextMatchingNode()) {
			  Tree match = matcher.getMatch();
			  
			  //nounPhrases.add(Sentence.listToString(match.yield()));
			  String nounPhrase = Sentence.listToString(match.yield());
			  Annotation doc = new Annotation(nounPhrase);
			  pipeline.annotate(doc);
			  List<CoreMap> nnSentences = doc.get(SentencesAnnotation.class);
			  for(CoreMap nnSentence: nnSentences)
			  {
				  //System.out.println("tokens num: " + nnSentences.size());
				  for(CoreLabel nnToken: nnSentence.get(TokensAnnotation.class))
				  {
					  
					  String nnWord = nnToken.getString(TextAnnotation.class);
					  String nnPOS = nnToken.getString(PartOfSpeechAnnotation.class);
					  if(nnPOS.equals("NN") || nnPOS.equals("NNS"))
					  {
						  nounInPhrase = nnWord;
						  if(!nnWord.equals("thing"))
							  nounsInPhrase.add(nnWord);
						  //System.out.println("is a " + nnWord);
					  }
				  }
			  }
			  
			  //System.out.println("np: " + Sentence.listToString(match.yield()));
		}
		
		for(CoreMap sentence: sentences)
		{
			CoreLabel prev = null;
			for(CoreLabel token: sentence.get(TokensAnnotation.class))
			{
				//Text of the token
				String word = token.getString(TextAnnotation.class);
				//POS tag of the token
				String pos = token.getString(PartOfSpeechAnnotation.class);
				
				partOfSpeech = pos;
				
				//System.out.println("word: " + word);
				//System.out.println("pos: " + pos);
				
				//If noun or noun phrase, add it to the concepts list
				//Includes tags (NN, NNS, NNP, NNPS)
				
				//If a verb, add it to the verbRelations list
				//Includes VB, VBD, VBG, VBN, VBP, VBZ
				if(pos.equals("VB") || pos.equals("VBD") || pos.equals("VBG") || pos.equals("VBN") 
						|| pos.equals("VBP") || pos.equals("VBZ"))
				{
					ontologyString += " --";
					if(word.equals("get") || word.equals("set"))
						ontologyString += "[has property]";
					verbRelations.add(word);
					ontologyString = ontologyString + word + "-->";
				}
				else
					ontologyString = ontologyString + " " + word;
				
				
				if(pos.equals("NN") || pos.equals("NNS"))
				{
					concepts.add(word);
					if(nounsInPhrase.contains(word))
					{
						ontologyString = ontologyString + "[is a " + word + "]";
					}
				}
				
				
				
				
				prev = token;
			}
			Tree tree = sentence.get(TreeAnnotation.class);
			
			SemanticGraph dependencies = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
		}
		Map<Integer, edu.stanford.nlp.dcoref.CorefChain> graph = document.get(CorefChainAnnotation.class);
		System.out.println("ontology String: " + ontologyString);
		return partOfSpeech;
	}
	
	
	
	public Tree getTree(final String template) {
		final List<CoreLabel> rawWordsOld = ExtractRelations.tokenizerFactory
			.getTokenizer(new StringReader(template)).tokenize();
		return ExtractRelations.lp.apply(rawWordsOld);
	    }

	
}
