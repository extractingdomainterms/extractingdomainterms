#!/bin/bash

# sh /Users/Neni/Documents/Research/Tools/srcml/run-srcml.sh /Users/Neni/Documents/Research/eclipseWorkspace/CodeCommentInconsistencies/rsc/cpp

# $1: dir with the files



#echo "### cleaning from previous runs... "
# clean previous runs:
#find "$1" \! -name "*\._*"  -name "*\.cpp\.xml"  -exec rm {} \;
#find "$1" \! -name "*\._*" -name "*\.c\.xml"   -exec rm {} \;
#find "$1" \! -name "*\._*" -name "*\.h\.xml"  -exec rm {} \;
#find "$1" \! -name "*\._*" -name "*\.java\.xml"  -exec rm {} \;


#echo "System statistics:"
#find "$1" \! -name "*\._*"  -name "*\.*" > "$1/loaf.txt"
#perl "$toolsDir/CLOC/cloc-1.56.pl" --list-file="$1/loaf.txt"

echo "### create list of source files to be parsed... "
### create list of source files to be parsed
find "$1" \! -name "*\._*" -name "*\.h" > "$1/lof-tmp"
find "$1" \! -name "*\._*" -name "*\.hpp" >> "$1/lof-tmp"
find "$1" \! -name "*\._*"  -name "*\.cpp" >> "$1/lof-tmp"
find "$1" \! -name "*\._*" -name "*\.c" >> "$1/lof-tmp"
find "$1" \! -name "*\._*" -name "*\.C" >> "$1/lof-tmp"
find "$1" \! -name "*\._*" -name "*\.cc" >> "$1/lof-tmp"
find "$1" \! -name "*\._*" -name "*\.java" >> "$1/lof-tmp"
cat "$1/lof-tmp" |sort > "$1/lof.txt"
rm "$1/lof-tmp"

#cd "/Users/Neni/Documents/Research/Tools/srcml/srcML-2012-06/"
cd "/home/alex/Documents/Research/Tools/srcml"

echo "### parsing source files with srcML... "
while read k;
do
    if [[ -e $k.xml ]]; then
        echo "Exists $k.xml"
    else
        if [[ $k == *\.java ]]; then
            echo "JAVA: $k"
            PROGRAM=`srcml  "$k" > "$k.xml"`
            cp $k.xml /home/alex/Desktop/srcTest/output
        elif [[ $k == *\.cpp ]]; then
            echo "C++: $k"
            PROGRAM=`srcml "$k" > "$k.xml"`
            cp $k.xml /home/alex/Desktop/srcTest/output
        elif [[ $k == *\.cc ]]; then
            echo "C++: $k"
            PROGRAM=`srcml  "$k" > "$k.xml"`
            cp $k.xml /home/alex/Desktop/srcTest/output
        elif [[ $k == *\.C ]]; then
            echo "C++: $k"
            PROGRAM=`srcml "$k" > "$k.xml"`
            cp $k.xml /home/alex/Desktop/srcTest/output
        elif [[ $k == *\.c ]]; then
            echo "C: $k"
            PROGRAM=`srcml  "$k" > "$k.xml"`
            cp $k.xml /home/alex/Desktop/srcTest/output
        elif [[ $k == *\.h ]]; then
            if [[ -e $(dirname $k)/$(basename $k .h).c ]]; then
                echo "C: $k"
                PROGRAM=`srcml "$k" > "$k.xml"`
                cp $k.xml /home/alex/Desktop/srcTest/output
            else
                echo "C++: $k"
                PROGRAM=`srcml "$k" > "$k.xml"`
                cp $k.xml /home/alex/Desktop/srcTest/output
            fi
        elif [[ $k == *\.hpp ]]; then
            if [[ -e $(dirname $k)/$(basename $k .h).c ]]; then
                echo "C: $k"
                PROGRAM=`srcml "$k" > "$k.xml"`
            else
                echo "C++: $k"
                PROGRAM=`srcml "$k" > "$k.xml"`
            fi
        fi

        $PROGRAM &

        if [[ `tail -1 "$k.xml"` == *\<\/unit\> ]]; then
            echo "Parsed fine!"
        else
            sleep 1
            if [[ `tail -1 "$k.xml"` == *\<\/unit\> ]]; then
                echo "Parsed fine after sleep 1!"
            else
                echo "Not done, will sleep..."
                sleep 10
                if [[ `tail -1 "$k.xml"` == *\<\/unit\> ]]; then
                    echo "Parsed fine after sleep!"
                else
                    echo "Not fine even after sleep! Killing: $k.xml"
                    mv "$k" "$k.err"
                    mv "$k.xml" "$k.xml.err"
                    if [[ -z `ps -e | grep "src2srcml"` ]]; then
                        echo "Process died alone."
                    else
                        pkill "src2srcml"
                    fi
                fi
            fi


        fi
    fi
done < "$1/lof.txt"

find "$1" \! -name "*\._*" -name "*\.h\.xml" -o  -name "*\.cpp\.xml" -o  -name "*\.c\.xml" -o  -name "*\.java\.xml" > "$1/loXf.txt"


echo "### DONE for $1"



exit;

