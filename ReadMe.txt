Tools used:
 - Stanford corenlp 3.6.0
 - slf4j 1.7.21
 - xom 1.2.10
 - Lucene 6.2.0

Projects analyzed:
 - fileZilla 3.0.0
 - WinMerge 2.14.0

Info:
    This code attempts to recreate the research project described in the papers "Natural Language Parsing of Program Element Names for Concept Extraction", "Towards the Extraction of Domain Concepts from the Identifiers", and "Automated Identifier Completion and Replacement."
    In general, the flow of the project has many steps.  The flow is:
	1.) Parse Filezilla and WinMerge using srcML to extract xml files structuring the code.
	2.) Extract all the identifiers from the xml files including class names, method names, and attribute names.  The structure should also be retained, so for example, an attribute in a class should know it's enclosing class name. 
	3.) Split the identifiers extracted in (2) by different notations, such as CamelCase and snake_case.  Also remove any Hungarian notation.
	4.) Create a candidate sentence for each identifier based on the rules described in the research papers.
	5.) Build an ontology from these candidate sentences.
	6.) Filter out the ontology concepts that are not the top weighted (for example top 100) terms.

What is left:
	So far, steps 1, 2, 3, 4, and part of 5 are completed.  What is left to do is to build the ontology and possibly display them in a nice format.  Currently it just prints to the console.  It also lacks structure and leaves multiple concepts.  Lastly, we need to filter the concepts(6).  Lucene is probably the best tool to do this.  Another tool I've seen is Luke, which is a nice gui for Lucene, however it seems a little dated.
